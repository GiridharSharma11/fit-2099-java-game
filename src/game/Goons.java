package game;

import edu.monash.fit2099.engine.*;

import java.util.ArrayList;
import java.util.List;

/** Class for Goons
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class Goons extends Actor {

	// Goons have 50 hitpoints and are always represented with a G
	/**
	 * Initializes an object for the Goons class. Adds player follow and ShoutInsult behaviour
	 * @param name name of the character
	 * @param subject an object of the Subplayer class
	 */
	public Goons(String name, Actor player) {
		super(name, 'G', 5, 50);
		addBehaviour(new ShoutInsult(this, player));
		addBehaviour(new FollowBehaviour(player));
		this.addSkill(CharacterSkills.MOONWALK);
		
	}
	
	// Creating an arraylist actionfactories of the ActionFactory class
	private List<ActionFactory> actionFactories = new ArrayList<ActionFactory>();

	/**
	 * Adds the behaviour in the Arraylist created 
	 * @param behaviour object of the ActionFactory class
	 */
	private void addBehaviour(ActionFactory behaviour) {
		actionFactories.add(behaviour);
	}
	
	/**
     * Select and return an action to perform on the current turn.
     *
     * By default, non-player Actors act randomly.  To change that, override this method.
     *
     * @param actions collection of possible Actions for this Actor
	 * @param map     the map containing the Actor
	 * @param display the I/O object to which messages may be written
	 * @return the Action to be performed
	 */
	@Override
	public Action playTurn(Actions actions, GameMap map, Display display) {
		for (ActionFactory factory : actionFactories) {
			Action action = factory.getAction(this, map);
			if(action != null)
				return action;
		}
		
		//Remove DropItemAction for the Goons so that it won't drop the key
		for (Action action: actions) {
			if(action instanceof DropItemAction) {
				actions.remove(action);
			}
		}
		
		return super.playTurn(actions,  map,  display);
	}
	
	/**
	 * Goon deals double the damage of the Grunt
	 */
	@Override
	protected IntrinsicWeapon getIntrinsicWeapon() {
		return new IntrinsicWeapon(10, "punches");
	}
	
	@Override
	public Actions getAllowableActions(Actor otherActor, String direction, GameMap map) {
		if(otherActor instanceof Player) {
			return new Actions(new AttackAction(otherActor, this));
		}
		else {
			return new Actions();
		}
	}
}

