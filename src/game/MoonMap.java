package game;

import java.util.List;

import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.GroundFactory;

/**
 * This class represents the Map for the moon
 * @author Thuta Lin
 *
 */
public class MoonMap extends GameMap{
	
	/**
	 * This is the constructor for initializing map of Moon
	 * @param groundFactory Factory to create Ground objects
	 * @param lines List of Strings representing rows of the map
	 */
	public MoonMap(GroundFactory groundFactory, List<String> lines) {
		// TODO Auto-generated constructor stub
		super(groundFactory, lines);
	}

}
