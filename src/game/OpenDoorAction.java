package game;

import java.util.List;

import game.Floor;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Exit;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Ground;
import edu.monash.fit2099.engine.Item;
import edu.monash.fit2099.engine.Location;
import edu.monash.fit2099.engine.Action;

/**
 * This class represents action to open the door
 * @author Thuta Lin
 */
public class OpenDoorAction extends Action {
	private Location doorLocation;
	private boolean hasKey = false;
	
	/**
	 * Constructor to initialise OpenDoorAction()
	 * 
	 */
	public OpenDoorAction() {
	}
	
	@Override
	/**
	 * Perform the Action.
	 *
	 * @param actor The actor performing the action.
	 * @param map The map the actor is on.
	 * @return a description of what happened that can be displayed to the user.
	 */
	public String execute (Actor actor, GameMap map) {
		List<Item> actorItems = actor.getInventory();
		
		for (int i = 0; i < actorItems.size(); i++) {
			if(actorItems.get(i).toString() == "Key") {
				hasKey = true;
				actor.removeItemFromInventory(actorItems.get(i));
				break;
			}
		}
		
		
		if(hasKey) {
			Location here = map.locationOf(actor);			
			
			Location doorLocation = null;
			
			for (Exit exit : here.getExits()) {
				Location destination = exit.getDestination();
				Ground adjacentGround = map.groundAt(destination);
				
				if(adjacentGround instanceof Door) {
					doorLocation = destination;
					break;
				}
			}
			
			
			map.add(new Floor(), doorLocation);
			return "The door was opened.";
		}
		else{
			return actor + " doesn't have a key to pass through";
		}
	}

	@Override
	/**
	 * Returns a descriptive string
	 * @param actor The actor performing the action.
	 * @return the text we put on the menu
	 */
	public String menuDescription(Actor actor) {
		return actor + " opens the door";
	}

	
	@Override
	/**
	 * Returns the key used in the menu to trigger this Action.
	 *
	 * @return The key we use for this Action in the menu.
	 */
	public String hotKey() {
		return "";
	}

}
