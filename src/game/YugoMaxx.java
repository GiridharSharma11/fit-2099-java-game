package game;

import java.util.Random;

import edu.monash.fit2099.engine.*;

/**
 * This is the class to represent the Character, Yugo Maxx
 * @author Giridhar Gopal Sharma
 * 
 */
public class YugoMaxx extends Actor {
	
	private Actor target;
	Random rand = new Random();
	
	/**
	 * The constructor for initializing this Actor
	 * @param name name of Yugo Maxx
	 * @param player the target of Yugo Maxx which will be a Player
	 */
	public YugoMaxx(String name, Actor player) {
		// TODO Auto-generated constructor stub	
		super(name, 'Y', 5, 50);
		this.target = player;
		this.addSkill(CharacterSkills.MOONWALK);
	}
	
	/**
	 * Yugo Maxx will just move around the map in random as long as the player is not around
	 * and if the player is around, It will attack the Player
	 */
	@Override
	public Action playTurn(Actions actions, GameMap map, Display display) {
		
		Location here = map.locationOf(this);
		Location there = map.locationOf(this.target);
		Location trueDestination = null;
		Exit trueExit = null;
		boolean setDestination = false;
		
		if(here.map().equals(there.map())) {
			
			//there = map.locationOf(this.target);
			
			for (Exit exit : here.getExits()) {
				
				Location destination = exit.getDestination();
				
				if(destination.equals(there)) {
					return new AttackAction(this, this.target);
				}
				
				if ((destination.canActorEnter((Actor)this)) && (rand.nextBoolean()) && (!setDestination)) 
				{
					trueDestination = destination;
					trueExit = exit;
				}
			}
			
			if(trueDestination != null && trueExit != null) {
				return new MoveActorAction(trueDestination, trueExit.getName());
			}
		}
		else 
		{
			for (Exit exit : here.getExits()) {
				
				Location destination = exit.getDestination();
				
				if ((destination.canActorEnter((Actor)this)) && (rand.nextBoolean()) && (!setDestination)) 
				{
					trueDestination = destination;
					trueExit = exit;
				}
			}
			
			if(trueDestination != null && trueExit != null) {
				return new MoveActorAction(trueDestination, trueExit.getName());
			}
		}
		return new SkipTurnAction();
	}
	
	/**
	 * Player will be able to do nothing to Yugo Maxx if the Player has no Water Pistol and if
	 * the Yugo Maxx has the Exoskeleton. If the Player has Water Pistol, it can be used to 
	 * destroy the Exoskeleton. If Yugo Maxx has no Exoskeleton, Player can attack Yugo Maxx.
	 */
	@Override
	public Actions getAllowableActions(Actor otherActor, String direction, GameMap map) {
		
		boolean hasAmmo = false;
		boolean hasExoskeleton = false;
		WaterPistol pistol = null;
		
		for(Item item: otherActor.getInventory()) {
			if(item.toString().equals("Water Pistol")) {
				WaterPistol waterPistol= (WaterPistol) item;
				if(waterPistol.hasAmmo()) {
					hasAmmo = true;
					pistol = waterPistol;
					break;
				}
			}
		}
		
		for(Item item: this.getInventory()) {
			if(item.toString().equals("Exoskeleton")) {
				hasExoskeleton = true;
			}
		}
		
		if(hasExoskeleton && hasAmmo) {
			return new Actions(new UseWaterPistol(this, pistol));
		}
		else if(hasExoskeleton && !hasAmmo ) {
			return new Actions();
		}
		else {
			return new Actions(new AttackAction(otherActor, this));
		}
		
	}
	
	/**
	 * Yugo Maxx deals double the damage of the Grunt
	 */
	@Override
	protected IntrinsicWeapon getIntrinsicWeapon() {
		return new IntrinsicWeapon(10, "punches");
	}
}
