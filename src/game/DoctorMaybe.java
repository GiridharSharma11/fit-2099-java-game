package game;

import java.util.ArrayList;

import edu.monash.fit2099.engine.*;
/**
 * Class that represents DoctorMaybe
 * @author Thiman Dulsara
 *
 */
public class DoctorMaybe extends Actor {
	
	/**
	 * Constructor to initialize DoctorMaybe
	 */
	public DoctorMaybe() {
		
		//Doctor Maybe has 25 hitpoints
		super("DoctorMaybe", 'D', 5, 25);
		addItemToInventory(Item.newInventoryItem("Rocket Body", 'B'));
	}
	
	@Override
	/**
     * Select and return an action to perform on the current turn.
     *
     * By default, non-player Actors act randomly.
     *
     * @param actions collection of possible Actions for this Actor
	 * @param map     the map containing the Actor
	 * @param display the I/O object to which messages may be written
	 * @return the Action to be performed
	 */
	public Action playTurn(Actions actions, GameMap map, Display display) {
		for (Action action: actions) {
			if (action instanceof AttackAction){
				return action;
			}
		}
		return new SkipTurnAction();
	}
	
	@Override
	public Actions getAllowableActions(Actor otherActor, String direction, GameMap map) {
		if(otherActor instanceof Player) {
			return new Actions(new AttackAction(otherActor, this));
		}
		else {
			return new Actions();
		}
	}
}