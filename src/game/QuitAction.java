package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;

/**
 * The action for Player to quit the game thus ending the Game
 * Extends from Action
 * @author T. D. Kumarage
 * @since 26/5/2019
 */
public class QuitAction extends Action{
	
	private SubPlayer player;
	
	/**
	 * The constructor for the Quit Action
	 * @param player the player performing this action
	 */
	public QuitAction(SubPlayer player) {
		// TODO Auto-generated constructor stub
		this.player = player;
	}
	
	/**
	 * The following action will trigger the hasExit private attribute in the SubPlayer
	 * class and next turn it will exit the Game
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		this.player.quit();
		return actor + " decides to quit the Game";
	}

	@Override
	public String menuDescription(Actor actor) {
		return actor + " exits the Game";
	}
	
	@Override
	public String hotKey() {
		return "";
	}

}
