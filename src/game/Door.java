package game;

import edu.monash.fit2099.engine.Actions;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.Ground;
import edu.monash.fit2099.engine.Location;
import edu.monash.fit2099.engine.Player;
import edu.monash.fit2099.engine.GameMap;

/**
 * The Door class extends from Ground. Reason to create Door class is to handle the required functionality of Door.
 * To allow or not allow player to pass depending on key availability.
 * @author Thiman T. D. Kumarage
 */

public class Door extends Ground {
	
	/**
	 * Constructor to initialize the Door object
	 */
	public Door() {
		// Door is represented with the '+' symbol
		super('+');
	}
	
		
	@Override
	/**
	 * Prevent entering the door
	 * 
	 * @param actor the Actor acting
	 * @return false
	 */
	public boolean canActorEnter(Actor actor) {
		return false;
	}
		
	@Override
	/**
	 * Returns an empty Action list.
	 *
	 * @param actor the Actor acting
	 * @param location the current Location
	 * @param direction the direction of the Ground from the Actor
	 * @return
	 */
	public Actions allowableActions(Actor actor, Location location, String direction){
		if(actor instanceof Player) {
		return new Actions(new OpenDoorAction());
		}
		return new Actions();
	}
		
	@Override
	/**
	 * Override this to implement terrain that blocks thrown objects but not movement, or vice versa
	 * @return true
	 */
	public boolean blocksThrownObjects() {
		return true;
	}
}
