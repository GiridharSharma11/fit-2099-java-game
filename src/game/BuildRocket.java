package game;

import java.util.*;

import edu.monash.fit2099.engine.*;

/** 
 * Class for Building Rocket
 * 
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class BuildRocket extends Action {
	
	/**
	 * Constructor initializes an object for the NinjaAction class
	 */
	public BuildRocket() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	/**
	 * Gives player Rocket Engine if player has plans
	 * 
	 * @param actor The actor performing the action.
	 * @param map The map where the player is currently positioned.
	 * 
	 * @return return a message to singnify if the action has completed or return if required items are missing
	 * 
	 */
	public String execute(Actor actor, GameMap map) {
		
		EarthMap earthMap = (EarthMap) map;
		
		List<Item> actorItems = actor.getInventory();
		
		boolean enginePresent = false;
		boolean bodyPresent = false;
		
		for (int i = 0; i < actorItems.size(); i++) {
			
			if(enginePresent && bodyPresent) {
				break;
			}
			
			if(actorItems.get(i).toString() == "Rocket Engine") 
			{
				enginePresent = true;
			}
			
			else if(actorItems.get(i).toString() == "Rocket Body")
			{
				bodyPresent = true;
			}
		}
		
		if(enginePresent && bodyPresent) {
			
			Location here = map.locationOf(actor);
			Location padLocation = null;

			for (Exit exit : here.getExits()) {
				Location destination = exit.getDestination();
				Ground adjacentGround = map.groundAt(destination);
				if(adjacentGround instanceof RocketPad) {
					padLocation = destination;
					break;
				}	
			}
			
			for(int i = 0; i < actorItems.size(); i++) {
				if(actorItems.get(i).toString().equals("Rocket Engine")) {
					actor.removeItemFromInventory(actorItems.get(i));
				}
				if(actorItems.get(i).toString().equals("Rocket Body")) {
					actor.removeItemFromInventory(actorItems.get(i));
				}
			}
			
			Item rocket = Item.newFurniture("Rocket", 'R');
			rocket.getAllowableActions().add(new MoveActorAction(earthMap.getDestination().at(0, 0), "to Moon!"));
			earthMap.addItem(rocket, padLocation.x(), padLocation.y());
			return "Rocket has been built successfully.";
		}
			
		else if(enginePresent || bodyPresent){
			if(!enginePresent) {
				return "Rocket Engine is missing!";
			}
			return "Rocket Body is missing!";
		}
		else {
			return "Rocket Engine and Rocket Body are missing!";
		}
	}

	@Override
	public String menuDescription(Actor actor) {
		return actor + " builds Rocket";
	}
	
	@Override
	public String hotKey() {
		return "";
	}
	
}
