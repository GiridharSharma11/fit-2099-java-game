package game;

import java.util.Arrays;
import java.util.List;

import edu.monash.fit2099.engine.*;

public class Application {

	public static void main(String[] args) {
		MetaWorld world = new MetaWorld(new Display());

		FancyGroundFactory groundFactory = new FancyGroundFactory(new Floor(), new Wall(), new Door(), new RocketPad(), new Water(), new MoonFloor());
		
		EarthMap earthMap;
		MoonMap moonMap;
		
		List<String> map2 = Arrays.asList(
				"Xooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo",
				"oooooooooooooooooooooo");
		moonMap = new MoonMap(groundFactory, map2);
		world.addMap(moonMap);
		
		List<String> map1 = Arrays.asList(
				".......................",
				"....#####....######....",
				"....#...#....#....#....",
				"....#...+....#....#....",
				"....#####....##+###....",
				".......................",
				"...................~~~.",
				"...................~~~.",
				".............X.........",
				".......................",
				".......................");
		earthMap = new EarthMap(groundFactory, map1, moonMap);
		world.addMap(earthMap);
		
		// Add rocket to the moon
		Item rocket = Item.newFurniture("Rocket", '^');
	    rocket.getAllowableActions().add(new MoveActorAction(earthMap.at(13, 8), "to Earth!"));
	    moonMap.addItem(rocket, 0, 0);
		
		// Creating Player and add to the Map
		SubPlayer player = new SubPlayer("Player", '@', 1, 100);
		//player.addItemToInventory(Item.newInventoryItem("Rocket Plans", 'e'));
		world.addPlayer(player, earthMap, 2, 2);
		
		// Creating Enemy for Earth Map
		Grunt grunt = new Grunt("Mongo", player);
		grunt.addItemToInventory(Item.newInventoryItem("Key", 'k'));
		earthMap.addActor(grunt, 0, 0);
		
		Grunt grunt2 = new Grunt("Norbert", player);
		grunt2.addItemToInventory(Item.newInventoryItem("Key", 'k'));
		earthMap.addActor(grunt2,  10, 10);
		
		Goons G = new Goons("Goon1", player);
		earthMap.addActor(G, 9, 2);
		
		DoctorMaybe D = new DoctorMaybe();
		earthMap.addActor(D, 6, 3);
		
		Ninja N = new Ninja("Ninja", player);
		earthMap.addActor(N, 9, 6);
		
		// Creating Q
		Q q = new Q();
		earthMap.addActor(q, 5, 6);
		
		
		// Creating Enemy for Moon Map
		Grunt grunt3 = new Grunt("Space Grunt", player);
		//grunt3.addItemToInventory(Item.newInventoryItem("Key", 'k'));
		moonMap.addActor(grunt3,  4, 4);
		
		Grunt grunt4 = new Grunt("Brother of Space Grunt", player);
		//grunt4.addItemToInventory(Item.newInventoryItem("Key", 'k'));
		moonMap.addActor(grunt4,  5, 5);
		
		//Create Yugo Maxx and add Exoskeleton to the Yugo Maxx
		YugoMaxx yugo = new YugoMaxx("Yugo Maxx", player);
		yugo.addItemToInventory(Item.newFurniture("Exoskeleton", 'e'));
		moonMap.addActor(yugo, 5, 7);
		
		// Add rocket plans in the locked room
		Item rocketPlans = new Item("Rocket Plans", 'p');
		earthMap.addItem(rocketPlans,16 , 2);
		
		// Add space Suit to the earth map
		Item spaceSuit = new Item("Space Suit", 'S');
		earthMap.addItem(spaceSuit, 2, 10);
		
		// Add Oxygen Dispenser
		Item o2Dispenser = Item.newFurniture("Oxygen Dispenser", 'D');
		o2Dispenser.getAllowableActions().add(new DispenseOxygen(earthMap.at(10, 10)));
		earthMap.addItem(o2Dispenser, 10, 10);
		
		// Add Water Pistol
		WaterPistol wp = new WaterPistol("Water Pistol", 'w');
		moonMap.addItem(wp, 0, 6);
		
		world.run();
	}
}
