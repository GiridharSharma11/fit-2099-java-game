package game;

import java.util.Random;

import edu.monash.fit2099.engine.*;

/**
 * This is the action of squirting water at the Yugo Maxx's Exoskeleton
 * @author Giridhar Gopal Sharma
 *
 */
public class UseWaterPistol extends Action {
	
	private Actor subject;
	private WaterPistol waterPistol;
	private Random rand = new Random();
	
	/**
	 * Constructor for initializing this Action 
	 * @param subject the target of this action
	 * @param pistol the water pistol item
	 */
	public UseWaterPistol(Actor subject, WaterPistol pistol) {
		// TODO Auto-generated constructor stub
		this.subject = subject;
		this.waterPistol = pistol;
	}
	
	@Override
	public String execute(Actor actor, GameMap map) {
		
		this.waterPistol.removeAmmo();
		
		if (rand.nextInt(10) < 3) {
			return actor + " misses " + subject + ".";
		}
		else {
			for(Item item: subject.getInventory()) {
				if(item.toString().equals("Exoskeleton")) {
					this.subject.removeItemFromInventory(item);
					break;
				}
			}
		}
		return actor + "destroys the Exoskeleton of " + subject + "."; 
	}

	@Override
	public String menuDescription(Actor actor) {
		return actor + " shoots water " + subject;
	}

	@Override
	public String hotKey() {
		return "";
	}
}
