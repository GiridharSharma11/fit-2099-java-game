package game;

import java.util.*;

import edu.monash.fit2099.engine.*;

/** 
 * Class for Ninja's throwing stun action 
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class GivePlans extends Action{
	
	private Actor actor;
	private Actor subject;
	
	/**
	 * Constructor initializes an object for the NinjaAction class
	 * @param actor player performing this action
	 * @param subject charcter with this action which is Q
	 */
	public GivePlans(Actor actor, Actor subject) {
		// TODO Auto-generated constructor stub
		this.actor = actor;
		this.subject = subject;
	}
	
	/**
	 * Gives player Rocket Engine if player has plans
	 * 
	 * @param actor The actor performing the action.
	 * @param map The map where the player is currently positioned.
	 * 
	 * @return return a message if player needs rocket plans or print farewell message
	 * 
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		List<Item> actorItems = actor.getInventory();
			
		for (int i = 0; i < actorItems.size(); i++) {
			if(actorItems.get(i).toString() == "Rocket Plans") 
			{
				actor.removeItemFromInventory(actorItems.get(i));
				actor.addItemToInventory(Item.newInventoryItem("Rocket Engine", 'e'));
				map.removeActor(this.subject);
				return "Player handed the Rocket Plan.\nQ returned Rocket Engine.\nFarewell from Q!";
					
			}
		}	
		return "I can give you something that will help, but I�m going to need the plans.";
	}

	@Override
	public String menuDescription(Actor actor) {
		return actor + " gives plans to " + subject;
	}

	@Override
	public String hotKey() {
		return "";
	}
}
