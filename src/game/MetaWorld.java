package game;

import java.util.List;

import edu.monash.fit2099.engine.*;

/**
 * This is the MetaWorld class
 * @author Thuta Lin
 */
public class MetaWorld extends World {
	
	/**
	 * The constructor for the class
	 * @param display
	 */
	public MetaWorld(Display display) {
		// TODO Auto-generated constructor stub
		super(display);
	}
	
	/**
	 * Getter for the maps List
	 * @return maps in the World
	 */
	public List<GameMap> getMaps() {
		return this.maps;
	}
	
	/**
	 * Run the world differently where the player will lose Oxygen on the moon map, will be 
	 * able to quit the game and win the game.
	 */
	@Override
	public void run() {
		
		if(player == null)
			throw new IllegalStateException();
		
		while (this.stillRunning()) {
			
			GameMap playersMap = actorLocations.locationOf(this.player).map();
			playersMap.draw(display);
			
			boolean hasQuit = false;
			
			for (Actor actor : actorLocations) {
				processActorTurn(actor);
				
				// Quit Message
				SubPlayer character = (SubPlayer) this.player;
				if(this.player.isConscious() && character.getQuit()) {
					hasQuit = true;
					break;
				}
			}
			
			if(hasQuit) {
				display.println(quitMessage());
				break;
			}
			
			// Game Over Message
			if(!this.stillRunning()) {
				display.println(endGameMessage());
				break;
			}
			
			playersMap = actorLocations.locationOf(player).map();
			boolean sleepingYugo = false;
			
			for(Item item: this.player.getInventory()) {
				if(item.toString().equals("Sleeping Yugo Maxx")) {
					sleepingYugo = true;
					break;
				}
			}
			
			// You Win Message
			if(playersMap.equals(getMaps().get(1)) && sleepingYugo) {
				display.println(winMessage());
				break;
			}
		}
	}
	
	protected String quitMessage() {
		return this.player + "has left the Game!!!";
	}
	
	protected String winMessage() {
		return "You won the Game!!!";
	}
	
	
	@Override
	protected void processActorTurn(Actor actor) {
		
		Location here = actorLocations.locationOf(actor);
		GameMap map = here.map();
		
		for(Item item: this.player.getInventory()) {
			if(item.toString().equals("Space Suit")) {
				if(item.getAllowableActions().size() < 1) {
					item.getAllowableActions().add(new DropItemAction(item));
				}
			}
		}
		
		super.processActorTurn(actor);
		
		if(actor.equals(this.player) && map.equals(this.maps.get(0))) 
		{
			display.println("At moon");
			
			for(Item item: this.player.getInventory()) {
				if(item.toString().equals("Space Suit")) {
					item.getAllowableActions().clear();
				}
			}
			
			if(!actorLocations.isAnActorAt(this.maps.get(0).at(0, 0))) 
			{
				display.println("On moon rocketpad");
				
				int count = 0;
				boolean minusOxygen = false;
				
				for(Item item: actor.getInventory()) 
				{
					
					if(item.toString().equals("Oxygen Tank")) 
					{
						
						OxygenTank tank = (OxygenTank) item;
						
						if(!minusOxygen) {
							
							if(tank.getOxygen() > 0) 
							{
								
								tank.reduceOxygen();
								minusOxygen = true;
								
								if(!(tank.getOxygen() > 0))
								{
									actor.removeItemFromInventory(item);
								}
								else {
									count = count + tank.getOxygen();
								}
							}
						}
						else 
						{
							count = count + tank.getOxygen();
						}
					}
				}
				
				display.println("Total Oxygen Point Remaining: "+ count);
				
				if(count <= 0) {
					MoveActorAction transport = new MoveActorAction(this.maps.get(1).at(13, 8), "to Earth!");
					transport.execute(actor, this.maps.get(1));
				}
			}
		}
	}

}
