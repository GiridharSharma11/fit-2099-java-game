package game;

import edu.monash.fit2099.engine.Action;
import edu.monash.fit2099.engine.Actor;
import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.Item;

public class FillWater extends Action {
	
	private WaterPistol pistol;
	
	public FillWater(WaterPistol item) {
		// TODO Auto-generated constructor stub
		this.pistol = item;
	}
	
	@Override
	public String execute(Actor actor, GameMap map) {
		if(!this.pistol.hasAmmo()) {
			this.pistol.addAmmo();
			return "Water Pistol has been reloaded";
		}
		else {
			return "Water Pistol is already reloaded";
		}
	}

	@Override
	public String menuDescription(Actor actor) {
		return actor + " reloads Water Pistol";
	}

	@Override
	public String hotKey() {
		return "";
	}
	

}
