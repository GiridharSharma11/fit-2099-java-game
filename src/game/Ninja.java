package game;

import java.util.ArrayList;
import java.util.List;

import edu.monash.fit2099.engine.*;
/** Class for Ninja 
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class Ninja extends Actor {
	
	// creating an arraylist for ActionFactory
	public List<ActionFactory> actionFactories = new ArrayList<ActionFactory>();
	
	/**
	 * Initializes an object for the Ninja class
	 * @param name name of the character
	 * @param player an target of Ninja
	 */
	public Ninja(String name, SubPlayer player) 
	{
		super(name, 'N', 5, 10);
		addBehaviour(new NinjaAction(player));
	}
	
	/**
	 * Adds the behaviour in the Arraylist created 
	 * @param behaviour object of the ActionFactory class
	 */
	private void addBehaviour(ActionFactory behaviour) {
		actionFactories.add(behaviour);
	}
	
	/**
     * Select and return an action to perform on the current turn.
     *
     * By default, non-player Actors act randomly.  To change that, override this method.
     *
     * @param actions collection of possible Actions for this Actor
	 * @param map     the map containing the Actor
	 * @param display the I/O object to which messages may be written
	 * @return the Action to be performed
	 */
	public Action playTurn(Actions actions, GameMap map, Display display) {
		for (ActionFactory factory : actionFactories) {
			Action action = factory.getAction(this, map);
			if(action != null)
				return action;
		}
		
		return new SkipTurnAction();
	}

	/**
     * Returns a collection of the Actions containing an AttackAction that the otherActor can do to the current Actor.
     *
     * @param otherActor the Actor that might be performing attack
	 * @param direction  String representing the direction of the other Actor
	 * @param map        current GameMap
	 * @return A collection of Actions.
	 */
	@Override
	public Actions getAllowableActions(Actor otherActor, String direction, GameMap map) {
		if(otherActor instanceof SubPlayer || otherActor instanceof Player) {
			return new Actions(new AttackAction(otherActor, this));
		}
		else 
		{
			return new Actions(); 
		}
	}

	
}
