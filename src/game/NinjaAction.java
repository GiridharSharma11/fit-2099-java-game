package game;

import edu.monash.fit2099.engine.*;
import java.util.Random;

/** 
 * Class for Ninja's throwing stun action 
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class NinjaAction extends Action implements ActionFactory{
	// creates an object target of the subplayer class
	private SubPlayer target;
	// create instance of Random class 
    Random rand = new Random();
	
    /**
	 * Initializes an object for the NinjaAction class
	 * @param subject an object of the Subplayer class
	 */
	public NinjaAction(SubPlayer subject) {
		this.target = subject;	
	}
	
	/**
	 * Returns a descriptive string of Ninja throwing a stun on the target
	 * @param actor The actor performing the action
	 * @param map The map where the player is currently positioned
	 * @return display message that the Ninja throws the stun at player
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		return actor + " throws stuns at " + target + ".";
	}
	
	/**
	 * Returns a descriptive string of message whether player is hit or stunned
	 * @param actor The actor performing the action.
	 * @param map The map where the player is currently positioned.
	 * @param hit returns true if player is stunned, false is Stun misses the player
	 * @return display message that the Stun hits the player or Stun misses the player 
	 */
	public String execute(Actor actor, GameMap map, boolean hit) {
		if (hit) {
			return "Stun hits the " + this.target;
		}
		else {
			return "Stun misses the " + this.target;
		}
	}
	
	/**
	 * Returns two maps showing the player skipping two turns if stunned by the ninja or else returns null(nothing)
	 * @param actor The actor performing the action.
	 * @param map The map where the player is currently positioned.
	 * @return two maps where player has skipped two chances if hit by the stun or else null if not hit
	 */
	
	@Override
	public Action getAction(Actor actor, GameMap map) {
		
		Location there = map.locationOf(target);
		Location here = map.locationOf(actor);
		
		if(!here.map().equals(there.map())) {
			return null;
		}
		
		if ( (Math.abs(here.x() - there.x())<=5) &&  (Math.abs(here.y() - there.y())<=5))

		  {
			if(this.target.checkStunned() == false) 
			{
				execute(actor, map);
				int a = rand.nextInt(10);
				if(a < 5) 
				{	
					System.out.println(execute(actor, map,true));
					target.setStunned(2);
				}
				else {
						System.out.println(execute(actor, map, false));
				}
				
			}
			
			int currentDistance = distance(here, there);
			for (Exit exit : here.getExits()) 
			{
				Location destination = exit.getDestination();
				if (destination.canActorEnter(actor)) 
				{
					int newDistance = distance(destination, there);
					if (newDistance > currentDistance) 
					{
						return new MoveActorAction(destination, exit.getName());
					}	
	
				}
			}
			
			
			return null;
			
		}
		else
			{
			return null;
			}
	}
	
	/**
	 * Returns the Manhattan distance
	 * @param a coordinates(x and y) of point a of Location class
	 * @param b coordinates(x and y) of point b of Location class
	 * @return the Manhattan distance between two points 
	 */
	private int distance(Location a, Location b){
			return Math.abs(a.x() - b.x()) + Math.abs(a.y() - b.y());
	}
	
	/**
	 * Returns a descriptive string
	 * @param actor The actor performing the action.
	 * @return the text we put on the menu
	 */
	@Override
	public String menuDescription(Actor actor) {
		return "";
	}
	
	/**
	 * Returns the key used in the menu to trigger this Action.
	 * @return The key we use for this Action in the menu.
	 */
	@Override
	public String hotKey() {
		return "";
	}
}
