package game;

import edu.monash.fit2099.engine.*;

import java.util.ArrayList;
import java.util.List;

/** Class for Grunt
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class Grunt extends Actor {
	
	
	// Grunts have 50 hitpoints and are always represented with a g
	/**
	 * Initializes an object for the Grunt class
	 * @param name name of the character
	 * @param subject the target of grunt
	 */
	public Grunt(String name, Actor player) {
		super(name, 'g', 5, 50);
		addBehaviour(new FollowBehaviour(player));
		this.addSkill(CharacterSkills.MOONWALK);
	}

	private List<ActionFactory> actionFactories = new ArrayList<ActionFactory>();
	
	/**
	 * Add action factory actions to actionFactories
	 * @param behaviour action factory action
	 */
	private void addBehaviour(ActionFactory behaviour) {
		actionFactories.add(behaviour);
	}
	
	@Override
	public Action playTurn(Actions actions, GameMap map, Display display) {
		for (ActionFactory factory : actionFactories) {
			Action action = factory.getAction(this, map);
			if(action != null)
				return action;
		}
		
		//Remove DropItemAction for the Grunts so that it won't drop the key
		for (Action action: actions) {
			if(action instanceof DropItemAction) {
				actions.remove(action);
			}
		}

		return super.playTurn(actions,  map,  display);
	}
	
	@Override
	public Actions getAllowableActions(Actor otherActor, String direction, GameMap map) {
		if(otherActor instanceof Player) {
			return new Actions(new AttackAction(otherActor, this));
		}
		else {
			return new Actions();
		}
	}
}
