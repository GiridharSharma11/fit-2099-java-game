package game;

import edu.monash.fit2099.engine.Item;

/**
 * This is the WaterPistol Item
 * @author Giridhar Gopal Sharma
 *
 */
public class WaterPistol extends Item {
	
	private boolean ammo;
	
	//This Item is represented as 'w' in the game
	/**
	 * Constructor for initializing the WaterPistol
	 * @param name "Water Pistol"
	 * @param displayChar character to represent the water pistol item
	 */
	public WaterPistol(String name, char displayChar) {
		// TODO Auto-generated constructor stub
		super(name,displayChar);
		this.ammo = false;
	}
	
	/**
	 * Check if the Pistol has ammo
	 * @return True if it has ammo or False otherwise
	 */
	public boolean hasAmmo() {
		return this.ammo;
	}
	
	/**
	 * Reload the Pistol
	 */
	public void addAmmo() {
		this.ammo = true;
	}
	
	/**
	 * Remove ammo from the Pistol
	 */
	public void removeAmmo() {
		this.ammo = false;
	}
}
