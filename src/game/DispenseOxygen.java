package game;
import edu.monash.fit2099.engine.*;

/**
 * This is the action of getting Oxygen from the Dispenser
 * @author Thuta Lin
 * @since 26/5/2019
 */
public class DispenseOxygen extends Action {
	
	private Location itemLocation;
	
	/**
	 * This is the constructor for Dispensing Oxygen
	 * @param location this is the location where Oxygen Tank will be created
	 */
	public DispenseOxygen(Location location) {
		// TODO Auto-generated constructor stub
		this.itemLocation = location;
	}
	
	/**
	 * When executed, new Oxygen Tank will be created at the location of
	 * Oxygen Dispenser unless Oxygen Tank is already created
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		
		boolean hasTank = false;
		
		for(Item item : this.itemLocation.getItems()) {
			if(item.toString().equals("Oxygen Tank")) {
				hasTank = true;
				break;
			}
		}
		
		if(!hasTank) {
			itemLocation.addItem(new OxygenTank("Oxygen Tank", 'T'));
			return "Oxygen Tank has been dropped";
		}
		else {
			return "Oxygen Tank is already created";
		}
	}

	@Override
	public String menuDescription(Actor actor) {
		return actor + " dispense Oxygen";
	}

	@Override
	public String hotKey() {
		return "";
	}

}
