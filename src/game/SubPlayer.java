package game;

import edu.monash.fit2099.engine.*;

/**
 * Subclass of player which can be stunned
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
 */
public class SubPlayer extends Player {
	
	private int stunTurn;
	private boolean hasExit;
	
	/**
	 * Constructor. Initializes a SubPlayer
	 * @param name Name of the SubPlayer/Player
	 * @param displayChar Symbol to represent Player
	 * @param priority Priority of Player
	 * @param hitPoints Health of Player
	 */
	public SubPlayer(String name, char displayChar, int priority, int hitPoints) {
		super(name, displayChar, priority, hitPoints);
		this.stunTurn= 0;
		hasExit = false;
		
	}
	
	/**
     * Will skip turn if the player has been stunned otherwise do possible actions
     *
     * @param actions collection of possible Actions for this Actor
	 * @param map     the map containing the Actor
	 * @param display the I/O object to which messages may be written
	 * @return the Action to be performed
	 */
	@Override
	public Action playTurn(Actions actions, GameMap map, Display display) {
		if(checkStunned()) {
			reduceStun();
			return super.playTurn(new Actions(new SkipTurnAction()), map, display);
		}
		else {
			actions.add(new QuitAction(this));
			return super.playTurn(actions, map, display);
		}
	}
	
	public void setStunned(int num) {
		this.stunTurn = num;
	}
	
	public int getStunned() {
		return this.stunTurn;
	}
	
	/**
     * check if the player has been stunned.
     * 
	 * @return true if the player is already stunned and false otherwise
	 */
	public boolean checkStunned() {
		if(getStunned() > 0) {
			return true;
		}
		else {
			return false;
		}	
	}
	
	/**
     * Reduce stun turn for player
     *
	 */
	public void reduceStun() {
		if(getStunned() > 0) {
			setStunned(getStunned()-1);
		}
	}
	
	public void quit() {
		this.hasExit = true;
	}
	
	public boolean getQuit() {
		return this.hasExit;
	}
	

}
