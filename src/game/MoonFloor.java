package game;

import edu.monash.fit2099.engine.*;

/**
 * This is the ground to represents the floor of the moon base. The reason for
 * creating this is to prevent the player from traveling in the moon base without
 * the Space Suit.
 * @author Thiman T. D. Kumarage
 * @since 26/05/2019
 *
 */
public class MoonFloor extends Ground {
	
	/**
	 * The constructor for initializing MoonFloor
	 */
	public MoonFloor() {
		// MoonFloor is represented with the 'o' symbol
		super('o');
	}
	
	/**
	 * Every actor needs either the skill, MOONWALK, or the space suit in 
	 * inventory to be able to travel/enter this Ground.
	 */
	@Override
	public boolean canActorEnter(Actor actor) {
		if(actor.hasSkill(CharacterSkills.MOONWALK)) {
			return true;
		}
		else {
			
			for(Item item: actor.getInventory()) {
				if(item.toString().equals("Space Suit")) {
					return true;
				}
			}
		}
		return false;
	}

}
