package game;

import java.util.List;

import edu.monash.fit2099.engine.GameMap;
import edu.monash.fit2099.engine.GroundFactory;

/**
 * This class represents the Map for the earth
 * @author Thuta Lin
 * @since 26/5/2019
 */
public class EarthMap extends GameMap {
	
	private MoonMap destination;
	
	/**
	 * This is the constructor for the earth map
	 * @param groundFactory Factory to create Ground objects
	 * @param lines List of Strings representing rows of the map
	 * @param map Map of the moon base
	 */
	public EarthMap(GroundFactory groundFactory, List<String> lines, MoonMap map) {
		// TODO Auto-generated constructor stub
		super(groundFactory, lines);
		this.destination = map;
	}
	
	/**
	 * This methods gets the destination from the Earth which will be
	 * a moon base in this case
	 * @return destination in this case is a moon base
	 */
	public MoonMap getDestination() {
		return this.destination;
	}

}
