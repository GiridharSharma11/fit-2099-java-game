package game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.monash.fit2099.engine.*;

/**
 * Class that represents the Q
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
 */
public class Q extends Actor {
	
	Random rand = new Random();
	
	/**
	 * Constructor. Initializes a Q.
	 */
	public Q() {
		// TODO Auto-generated constructor stub
		super("Q", 'Q', 6, 100);
	}
	
	/**
     * Returns Q's action for player.
     *
     * @param otherActor the Actor that might be performing attack
	 * @param direction  String representing the direction of the other Actor
	 * @param map        current GameMap
	 * @return Talk actions and Give Plans actions for player and no actions for other characters
	 */
	@Override
	public Actions getAllowableActions(Actor otherActor, String direction, GameMap map) {
		if(otherActor instanceof Player) {
			Actions actionObj = new Actions(new GivePlans(otherActor, this));
			actionObj.add(new Talk(otherActor,this));
			return actionObj;
		}
		else 
		{
			return new Actions();
		}
	}
	
	/**
     * Q moves when play turn and may skip turn randomly.
     * 
     * @param actions collection of possible Actions for this Actor
	 * @param map     the map containing the Actor
	 * @param display the I/O object to which messages may be written
	 * @return MoveActorAction or SkipTurnAction
	 */
	@Override
	public Action playTurn(Actions actions, GameMap map, Display display) {
		
		Location here = map.locationOf(this);
		
		Location trueDestination = null;
		Exit trueExit = null;
		
		for (Exit exit : here.getExits()) {
			Location destination = exit.getDestination();
			
			if (destination.canActorEnter(this) && rand.nextBoolean()) 
			{
				trueDestination = destination;
				trueExit = exit;
				break;
			}
		}
		
		if(trueDestination == null) {
			return new SkipTurnAction();
		}
		else {
			return new MoveActorAction(trueDestination, trueExit.getName());
		}
		/**
		Location here = map.locationOf(this);
		boolean entered = false;
		Location lastlocation = null;
		Exit lastexit = null;
		
		for (Exit exit : here.getExits()) {
			Location destination = exit.getDestination();
			if (destination.canActorEnter(this)) {
				lastlocation = destination;
				lastexit = exit;
				if(rand.nextInt(1) == 1) {
					lastlocation = destination;
					lastexit = exit;
					break;
				}
			}
		}
		
		if(lastlocation == null || lastexit == null) {
			return new SkipTurnAction();
		}
		return new MoveActorAction(lastlocation, lastexit.getName());
		**/
	}
}

