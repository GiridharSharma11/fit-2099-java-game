package game;

import edu.monash.fit2099.engine.*;

/**
 * This the subclass of Ground class to represent Water
 * @author Giridhar Gopal Sharma
 *
 */
public class Water extends Ground {
	
	/**
	 * Constructor for this class
	 */
	public Water() {
		// This ground is represented as '~' on the map
		super('~');
	}
	
	
	@Override
	public boolean canActorEnter(Actor actor) {
		return false;
	}
	
	/**
	 * Allow Player to fill the WaterPistol if Player has the item
	 */
	@Override
	public Actions allowableActions(Actor actor, Location location, String direction){
		if(actor instanceof Player) {
			for(Item item: actor.getInventory()) {
				if(item.toString().equals("Water Pistol")) {
					return new Actions(new FillWater((WaterPistol)item));
				}
			}
		}
		return new Actions();
	}
}
