package game;

import edu.monash.fit2099.engine.*;

/** Class for ShoutInsult
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
*/
public class ShoutInsult extends Action implements ActionFactory{
		
		private Actor subject;
		private String insult;
		
		/**
		 * Initializes an object for the ShoutInsult class
		 * @param actor an object of the Actor class(doing the action)
		 * @param subject an object of the Actor class(target)
		 */
		public ShoutInsult(Actor actor, Actor subject) {
			this.subject = subject;
		}
		
		// Array storing all the insults 
		private String[] insults = {"Amateaur player haha !!", "Your aim is so weak !!!", "Noob Player haha !!"};
		
		/**
		 * Returns a descriptive string
		 * @param actor The actor performing the action.
		 * @return the text we put on the menu
		 */
		@Override
		public String menuDescription(Actor actor) {
			return actor + " shouts " + getInsult() + " to " + subject;
		}
		
		/**
		 * Returns the key used in the menu to trigger this Action.
		 * @return The key we use for this Action in the menu.
		 */
		@Override
		public String hotKey() {
			return "";
		}
		
		
		@Override
		public String execute(Actor actor, GameMap map) {
			return menuDescription(actor);
		}
		
		/**
		 * Returns an insult from the array if random fulfills the if condition else null(nothing)
		 * @param actor The actor performing the action.
		 * @param map The map where the player is currently positioned.
		 * @return an insult from the array if random fulfills the if condition else null
		 */
		@Override
		public Action getAction(Actor actor, GameMap map) {
			if ( Math.random() < 0.1) { // Since it has a 10% chance of saying an insult
				
				int index = (int)(Math.round(Math.random()*10)%( insults.length - 1 ));
				this.insult = insults[index];
				return this;
					
			}	
			else {
				return null;
			}
		}
		
		
		protected String getInsult() {
			return this.insult;
		}
}


