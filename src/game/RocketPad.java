package game;

import edu.monash.fit2099.engine.*;

/**
 * Rocket Pad class where player can build Rocket
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
 */
public class RocketPad extends Ground {
	
	/**
	 * Constructor. Initializes a RocketPad
	 */
	public RocketPad() {
		// TODO Auto-generated constructor stub
		super('X');
		
	}
	
	/**
	 * Returns BuildRocket action for player.
	 *
	 * @param actor the Actor acting
	 * @param location the current Location
	 * @param direction the direction of the Ground from the Actor
	 * @return BuildRocket action is returned otherwise return no actions
	 */
	@Override
	public Actions allowableActions(Actor actor, Location location, String direction){
		if(actor instanceof Player) {
			for(Item item :location.getItems()) {
				if(item.toString().equals("Rocket")) {
					return new Actions();
				}
			}
			return new Actions(new BuildRocket());
		}
		return new Actions();
	}
	
	public boolean canActorEnter(Actor actor) {
		if(actor instanceof Player) {
			return true;
		}
		return false;
	}
	
}
