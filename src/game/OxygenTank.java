package game;

import edu.monash.fit2099.engine.*;

/**
 * This is class that represents the Oxygen Tank
 * @author Thuta Lin
 * @since 26/5/2019
 */
public class OxygenTank extends Item {
	
	// This attribute represents the remaining Oxygen in the tank
	private int oxygenUnit;
	
	/**
	 * Constructor for the Oxygen Tank
	 * @param name of this item
	 * @param displayChar character to represent this item on the map
	 */
	public OxygenTank(String name, char displayChar) {
		// TODO Auto-generated constructor stub
		super(name, displayChar);
		this.oxygenUnit = 10;
	}
	
	/**
	 * This method gets the remaining Oxygen in the tank
	 * @return currently remaining Oxygen
	 */
	public int getOxygen() {
		return this.oxygenUnit;
	}
	
	/**
	 * This method reduces the Oxygen in the tank by one
	 */
	public void reduceOxygen() {
		this.oxygenUnit = getOxygen()-1;
	}

}
