package game;

import java.util.List;

import edu.monash.fit2099.engine.*;

/**
 * Classes for Talking action between Player and Q
 * @author Giridhar Gopal Sharma
 * @author Thuta Lin
 * @author Thiman Thulith Dulsara Kumarage
 */
public class Talk extends Action{
	
	private Actor subject;
	private Actor actor;
	
	/**
	 * Constructor. Initializes a talk action class
	 * @param actor Player 
	 * @param subject Q will be a subject
	 */
	public Talk(Actor actor, Actor subject) {
		// TODO Auto-generated constructor stub
		this.actor = actor;
		this.subject = subject;
	}
	
	
	/**
	 * Returns a response depends on player having Rocket Plans or not.
	 * 
	 * @param actor The actor performing the action.
	 * @param map The map where the player is currently positioned.
	 * 
	 * @return string of Q's response message
	 * 
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		List<Item> actorItems = actor.getInventory();
			
		for (int i = 0; i < actorItems.size(); i++) {
			if(actorItems.get(i).toString() == "Rocket Plans") 
			{
				return "Hand them over, I don�t have all day!";	
			}
		}	
		return "I can give you something that will help, but I�m going to need the plans.";
	}
	
	@Override
	public String menuDescription(Actor actor) {
		return actor + " talks to " + subject;
	}

	@Override
	public String hotKey() {
		return "";
	}
}
